isoquery (3.2.6-1) unstable; urgency=medium

  * New upstream version 3.2.6
    - Update test data to current iso-codes 4.8.0, so that tests no longer
      fail. Closes: #998587
  * Support 'nocheck' in $(DEB_BUILD_OPTIONS)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 15 Nov 2021 12:28:08 +0100

isoquery (3.2.5-1) unstable; urgency=medium

  * New upstream version 3.2.5
  - Update test to match French translation change in iso-codes.
    Closes: #991653
  * Mark isoquery Multi-Arch: foreign.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #949771)
  * Add gbp.conf for DEP-14
  * Update Standards-Version to 4.6.0, no changes needed
  * Update d/copyright
  * Update d/watch to v4

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 26 Aug 2021 20:52:02 +0200

isoquery (3.2.4-1) unstable; urgency=medium

  * New upstream version 3.2.4
  - Update test to match French translation change in iso-codes.
    Closes: #963371
  * Use debhelper v13
  * Do not hardcode --as-needed linker flag
  * Update to Standards-Version 4.5.1
  * Update d/copyright
  * Set Rules-Requires-Root to no

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 28 Dec 2020 17:24:11 +0100

isoquery (3.2.3-1) unstable; urgency=medium

  * New upstream version 3.2.3
    - Fix FTBFS due to changed po4a string extraction in rst manpage.
      Closes: #906475

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sat, 18 Aug 2018 22:22:21 +0200

isoquery (3.2.2-2) unstable; urgency=medium

  * Use debhelper v11
  * Update d/copyright
  * Update Vcs-URLs to salsa.d.o
  * Update Standards-Version to 4.1.3, no changes needed

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sun, 21 Jan 2018 16:40:13 +0100

isoquery (3.2.2-1) unstable; urgency=medium

  * New upstream version 3.2.2
    - Fix FTBFS due to missing newline character in German translation.
      Closes: #876855
  * Use debhelper v10
  * Update d/copyright
  * Remove dh-autoreconf from Depends
  * Remove patch, has been applied upstream
  * Update Standards-Version to 4.1.0

 -- Dr. Tobias Quathamer <toddy@debian.org>  Tue, 26 Sep 2017 14:26:46 +0200

isoquery (3.2.1-2) unstable; urgency=medium

  * Fix FTBFS in test suite after iso-codes translation update.
    Thanks to Chris Lamb <lamby@debian.org> (Closes: #862301)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 11 May 2017 21:00:21 +0200

isoquery (3.2.1-1) unstable; urgency=medium

  * Imported Upstream version 3.2.1
    - Fix FTBFS due to recent changes in iso-codes. Thanks to
      Lucas Nussbaum for the bug report. Closes: #835775
  * Update watch file

 -- Dr. Tobias Quathamer <toddy@debian.org>  Tue, 30 Aug 2016 22:00:57 +0200

isoquery (3.1.0-1) unstable; urgency=medium

  * Imported Upstream version 3.1.0
    - Added French translation. Closes: #830161
    - Added French manpage translation. Closes: #830163

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 13 Jul 2016 14:39:26 +0200

isoquery (3.0.1-1) unstable; urgency=medium

  * Imported Upstream version 3.0.1
    - Update Build-Depends for new codebase
    - Update package description
    - Update copyright with new download location
    - Update watch file with new download location
    - Update bash completion script
  * Add homepage
  * Use all hardening flags
  * Update Standards-Version to 3.9.8, no changes needed
  * Include locale generation in d/rules to enable testing

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 10 Jun 2016 11:44:09 +0200

isoquery (2.0-6) unstable; urgency=medium

  * Really remove obsolete conffile this time.
    Thanks to Jakub Wilk for insisting ;-) (Closes: #811483)
  * Fix git URL

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 27 Jan 2016 16:26:12 +0100

isoquery (2.0-5) unstable; urgency=medium

  * Add d/maintscript to remove obsolete conffile. (Closes: #811483)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 22 Jan 2016 10:05:40 +0100

isoquery (2.0-4) unstable; urgency=medium

  * Rebuild to install bash completion into correct directory
  * Update maintainer name
  * Fix syntax of d/copyright
  * Use HTTPS for git URLs
  * Switch to python3-docutils

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 15 Jan 2016 22:20:20 +0100

isoquery (2.0-3) unstable; urgency=medium

  * Add bash completion for isoquery.
    Thanks to Ben Finney <ben+debian@benfinney.id.au> (Closes: #762667)
  * Use DEP-5 copyright
  * Update Standards-Version to 3.9.6, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Sat, 18 Oct 2014 23:34:11 +0200

isoquery (2.0-2) unstable; urgency=medium

  * Update Vcs-URLs to Debian packaging
  * Build-Depend on libgee-0.8-dev, fixes FTBFS.
    Thanks to David Suárez <david.sephirot@gmail.com> (Closes: #755319)

 -- Tobias Quathamer <toddy@debian.org>  Tue, 22 Jul 2014 10:05:49 +0200

isoquery (2.0-1) unstable; urgency=low

  * Imported Upstream version 2.0
    - Significantly faster than previous version. Closes: #649675
    - Correctly handles ISO 639-3 again. Closes: #698882
  * Use debhelper v9
  * Update URLs for git repository
  * Update Standards-Version to 3.9.5, no changes needed
  * Update dependencies of package
  * Update URL for debian/watch
  * Update copyright years
  * Use dh-autoreconf during build
  * Reduce debian/rules, documentation is now handled correctly by debhelper
  * Add AUTHORS to installed docs

 -- Tobias Quathamer <toddy@debian.org>  Fri, 30 May 2014 21:32:46 +0200

isoquery (1.7-1) unstable; urgency=low

  * New upstream release
    - Fix a UnicodeEncodeError in version output, thanks to David Prévot.
      Closes: #650746
    - Use standard distutils setup.py file for building and installing
      the package, remove waf build system completely. Closes: #654476
  * Remove transitional package countrycodes
  * Remove useless README file from package, thanks to David Prévot.
    Closes: #653759
  * Update copyright years
  * Build using dh_python2, patch taken from Ubuntu
  * Update download location in debian/watch and debian/copyright

 -- Tobias Quathamer <toddy@debian.org>  Tue, 24 Jan 2012 00:08:14 +0100

isoquery (1.5-1) unstable; urgency=low

  * New upstream release
    - If the locale does not exist for the given ISO standard,
      print a warning message and use the untranslated (thus
      English) strings. Closes: #624147
    - Review isoquery manpage, thanks to David Prévot. Closes: #627577
    - New manpage translation to French, thanks to David Prévot.
      Closes: #628841
    - Updated translations:
      + Swedish, thanks to Martin Bagge. Closes: #628863
      + Russian, thanks to Yuri Kozlov. Closes: #628966
      + Czech, thanks to Michal Simunek. Closes: #629112
      + French, thanks to Christian Perrier. Closes: #629164
      + Danish, thanks to Joe Hansen. Closes: #630246
  * Use debhelper v8
  * Update years in copyright
  * Update to Standards-Version 3.9.2, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Tue, 14 Jun 2011 00:51:55 +0200

isoquery (1.4-1) unstable; urgency=low

  * New upstream release
    - New translation to Vietnamese, thanks to Clytie Siddall.
      Closes: #598618

 -- Tobias Quathamer <toddy@debian.org>  Sat, 02 Oct 2010 21:53:20 +0200

isoquery (1.3-1) unstable; urgency=low

  * New upstream release
    - New translation to Portuguese, thanks to Américo Monteiro.
      Closes: #592431
    - New manpage translation to Portuguese, thanks to Américo Monteiro.
      Closes: #592433
  * Add new package countrycodes as a transitional dummy package. This
    eases the removal of the obsolete package countrycodes in the squeeze
    release. Closes: #506189

 -- Tobias Quathamer <toddy@debian.org>  Sun, 15 Aug 2010 18:32:01 +0200

isoquery (1.2-1) unstable; urgency=low

  * New upstream release
    - Fix invalid syntax for Python 2.4. Thanks to Cristian Ionescu-Idbohrn
      for the bug report and patch. Closes: #591035
  * Update watch file

 -- Tobias Quathamer <toddy@debian.org>  Sat, 31 Jul 2010 18:05:00 +0200

isoquery (1.1-1) unstable; urgency=low

  * New upstream release
    - French translation update, thanks to Christian Perrier. Closes: #589554
    - New translation to Russian, thanks to Yuri Kozlov. Closes: #589521
    - New translation to Czech, thanks to Michal Šimůnek. Closes: #589719
    - New translation to Danish, thanks to Joe Hansen. Closes: #589861
    - New translation to Spanish, thanks to Omar Campagne. Closes: #590564
  * Add python-docutils and po4a to Build-Depends-Indep for manpage
    generation with rst2man and translation with po4a
  * Update to Standards-Version 3.9.1, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Wed, 28 Jul 2010 10:45:25 +0200

isoquery (1.0-1) experimental; urgency=low

  * New upstream release
    - Complete rewrite in Python
  * Add python-support to Build-Depends-Indep
  * Add python-lxml to Depends
  * Switch to dpkg source format 3.0 (quilt)
  * Update to Standards-Version 3.9.0, no changes needed

 -- Tobias Quathamer <toddy@debian.org>  Wed, 30 Jun 2010 21:48:02 +0200

isoquery (0.18-1) unstable; urgency=low

  * New upstream release
  * Switch to debhelper v7, reduce debian/rules
  * Update debian/copyright and replace (C) with ©
  * Update to Standards-Version 3.8.1, no changes needed
  * Add ${misc:Depends} to debian/control to fix a Lintian warning

 -- Tobias Quathamer <toddy@debian.org>  Mon, 01 Jun 2009 07:52:40 +0200

isoquery (0.17-1) unstable; urgency=low

  * New upstream release
  * Change maintainer's last name to Quathamer

 -- Tobias Quathamer <toddy@debian.org>  Wed, 26 Nov 2008 10:12:47 +0100

isoquery (0.16-1) unstable; urgency=low

  * New upstream release
  * Update to Standards-Version 3.8.0, no changes needed

 -- Tobias Toedter <toddy@debian.org>  Tue, 01 Jul 2008 17:37:53 +0200

isoquery (0.15-1) unstable; urgency=low

  * New upstream release

 -- Tobias Toedter <toddy@debian.org>  Mon, 02 Jun 2008 19:02:42 +0200

isoquery (0.14-1) unstable; urgency=low

  * New upstream release
    - Fix bug which could cause isoquery to throw an Glib::ConvertError.
      Thanks to George Danchev for the bug report. Closes: #474534
  * Use hardening-wrapper on m68k as well
  * Change Maintainer's e-mail address
  * Correct URL of public git repository
  * Remove DM upload rights, they are no longer needed

 -- Tobias Toedter <toddy@debian.org>  Thu, 24 Apr 2008 10:58:10 +0200

isoquery (0.13-1) unstable; urgency=low

  * New upstream release
  * Build-Depend on hardening-wrapper

 -- Tobias Toedter <t.toedter@gmx.net>  Wed, 12 Mar 2008 09:20:28 +0100

isoquery (0.12-1) unstable; urgency=low

  * New upstream version
    - Add #include statements required by GCC 4.3. Thanks to
      Martin Michlmayr for the bug report and patch. Closes: #462206

 -- Tobias Toedter <t.toedter@gmx.net>  Thu, 24 Jan 2008 12:29:45 +0000

isoquery (0.11-1) unstable; urgency=low

  * New upstream version

 -- Tobias Toedter <t.toedter@gmx.net>  Wed, 09 Jan 2008 11:01:43 +0100

isoquery (0.10-1) unstable; urgency=low

  * New upstream version
  * Recommend iso-codes. Thanks to Tony Mancill and Trent W. Buck for
    pointing this out. Closes: #458614
  * Update copyright years
  * Update Vcs-* fields to new git repository

 -- Tobias Toedter <t.toedter@gmx.net>  Wed, 02 Jan 2008 12:00:58 +0100

isoquery (0.9-2) unstable; urgency=low

  * Mention that src/gettext.h is licensed under LGPL in debian/copyright.
    Thanks to Joerg Jaspert for pointing this out.

 -- Tobias Toedter <t.toedter@gmx.net>  Fri, 28 Dec 2007 15:06:44 +0100

isoquery (0.9-1) unstable; urgency=low

  * Initial release (Closes: #453445)

 -- Tobias Toedter <t.toedter@gmx.net>  Thu, 22 Nov 2007 10:50:30 +0100
